package uni_rostock.informatik.sema.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.jena.atlas.json.JsonBuilder;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;

import uni_rostock.informatik.sema.rest.exception.TooMuchDataInput;
import uni_rostock.informatik.sema.sparql.SparqlQuery;

@Path("imagerecog")
public class RestHandlerImageRecog extends RestHandler {

    /**
     * Method handling HTTP GET requests. The returned object will be sent to the
     * client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String callFunction(Map<String, Object> request) {
	String sex = null, rootElement = null;
	ArrayList<String> scenes = null;
	sex = extractGender(request);
	rootElement = extractString(request, "fashionElement").toUpperCase();
	scenes = extractSceneNames(request, "scenes");

	if (request.size() > 0)
	    throw new TooMuchDataInput(request);

	// if this element is empty, set it to root-Element of Image-Recognition-Tree
	if (rootElement == null || rootElement == "")
	    rootElement = "IMAGERECOGNITION";

	JsonValue json;
	
	if (scenes != null && scenes.size() != 0) {
	    json = getPossibleFashion((List<String>) scenes, rootElement, sex);
	} else
	    json =getPossibleFashion(null, rootElement, sex);
	
	
	return json.toString();
    }

    public JsonValue getPossibleFashion(List<String> scenes, String rootElement, String sex) {
	return sq.execImageRecogQuery(scenes, rootElement, sex);
    }
}
