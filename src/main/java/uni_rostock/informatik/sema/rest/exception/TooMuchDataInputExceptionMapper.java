package uni_rostock.informatik.sema.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class TooMuchDataInputExceptionMapper implements ExceptionMapper<TooMuchDataInput> {

    @Override
    public Response toResponse(TooMuchDataInput exception) {
	ErrorMessage errorMessage = new ErrorMessage(exception.wrongDataTypes, Status.BAD_REQUEST.getStatusCode(), "You provided invalid information within the request. Please check your request.");
		
	return Response.status(Status.BAD_REQUEST).entity(errorMessage).build();
    }
    
    

}
