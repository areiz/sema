package uni_rostock.informatik.sema.rest.exception;

public class WrongDataType extends RuntimeException{

    public WrongDataType(String message) {
	super(message);
    }
}
