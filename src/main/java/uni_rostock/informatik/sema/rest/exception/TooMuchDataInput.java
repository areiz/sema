package uni_rostock.informatik.sema.rest.exception;

import java.util.Map;
import java.util.Map.Entry;

public class TooMuchDataInput extends RuntimeException{

    /**
     * 
     */
    public String wrongDataTypes;
    private static final long serialVersionUID = -6222658695010647351L;

    public TooMuchDataInput(Map<String, Object> request) {
	super("Too much data in Input string");
	StringBuilder sb = new StringBuilder();
	for (Entry<String, Object> entry : request.entrySet()) {
	    sb.append("Invalid Input Variable: ");
	    sb.append(entry.getKey());
	    sb.append("; ");
	}
	this.wrongDataTypes = sb.toString();
	

	
    }
    
}
