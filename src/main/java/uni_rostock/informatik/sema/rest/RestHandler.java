package uni_rostock.informatik.sema.rest;

import java.util.ArrayList;
import java.util.Map;

import uni_rostock.informatik.sema.sparql.SparqlQuery;

public class RestHandler {
    protected SparqlQuery sq;
    public RestHandler() {
	sq = new SparqlQuery();
    }

    
    /***
     * Extracts the Value-Array out of a {@link Map} and removes it out of the
     * request afterwards. If the item is not accessible, it returns null.
     * 
     * @param request The input value of the Post-Method
     * @param mapKey  A String representing the Key of the JSON Key-Value Pair
     * @return The Value out of the Key-Value pair
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected ArrayList<String> extractSceneNames(Map<String, Object> request, String mapKey) {
	ArrayList<String> list = new ArrayList<String>();

	if (request.containsKey(mapKey)) {
	    for (Map map : (ArrayList<Map>) request.get(mapKey)) {
		list.add(((String) map.get("name")).toLowerCase());
	    }

	    request.remove(mapKey);
	} else
	    return null;
	return list;

    }
    

    /***
     * Extracts the Value-String out of a {@link Map} and removes it out of the
     * request afterwards. If the item is not accessible, it returns null.
     * 
     * @param request The input value of the Post-Method
     * @param mapKey  A String representing the Key of the JSON Key-Value Pair
     * @return The Value out of the Key-Value pair
     */
    protected String extractString(Map<String, Object> request, String mapKey) {
	String value;
	if (request.containsKey(mapKey)) {
	    value = request.get(mapKey).toString().toLowerCase();
	    request.remove(mapKey);
	} else
	    value = null;
	return value;
    }

    /***
     * Extracts and converts the gender out of a {@link Map} and removes it out of
     * the request afterwards. If there is more than one gender (male AND female),
     * the value is set to null as this is already implemented as "no
     * gender-filtering"
     * 
     * @param request Input Value of the Post-method
     * @param mapKey  A String representing the Key of the JSON Key-Value Pair
     * @return The Value out of the Key-Value pair
     */
    protected String extractGender(Map<String, Object> request) {
	String value;
	final String mapKey = "genders";
	if (request.containsKey(mapKey)) {
	    value = request.get(mapKey).toString().toLowerCase();

	    if (value.contains("[female]"))
		value = "female";
	    else if (value.contains("[male]"))
		value = "male";
	    else
		value = null;
	    request.remove(mapKey);
	} else
	    value = null;
	return value;
    }
}
