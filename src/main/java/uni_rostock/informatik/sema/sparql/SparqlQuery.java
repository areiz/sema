package uni_rostock.informatik.sema.sparql;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.jena.atlas.json.JsonBuilder;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;
import org.apache.jena.atlas.json.JsonVisitor;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdfconnection.RDFConnection;

import org.apache.jena.util.FileManager;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ser.std.JsonValueSerializer;

public class SparqlQuery {

    private Model model;
    private RDFConnection virtuosoCon;

    public SparqlQuery() {

	System.out.println("SPARQL-QUERY Class created!");
	FileManager.get().addLocatorClassLoader(getClass().getClassLoader());
	String owlLocationString = getClass().getClassLoader().getResource("Fashion.owl").getPath();

	this.model = FileManager.get().loadModel(owlLocationString);

//	try {
//	    DriverManager.registerDriver(new virtuoso.jdbc4.Driver());
//	} catch (SQLException e) {
//	    // TODO Auto-generated catch block
//	    e.printStackTrace();
//	}
//	virtuosoCon = VirtuosoSystemService.connectVirtuoso("localhost", 8890, 1111);
	// this.model = virtuosoCon.fetch("fashion");

	/*
	 * Reasoning Capabilities
	 * 
	 * Model data = FileManager.get().loadModel(owlLocationString); Reasoner
	 * reasoner = ReasonerRegistry.getRDFSReasoner(); reasoner.bindSchema(data);
	 * System.out.println("Run Reasonser"); this.model =
	 * ModelFactory.createInfModel(reasoner, data);
	 * System.out.println("inferred Model created");
	 */

    }

    public JsonValue queryFashionOntology(String type, List<String> scenes, String rootElement, String gender) {

	String queryString = SparqlQueryHelper.buildFashionQuery(scenes, rootElement, gender, type);
	return queryOntology(scenes, queryString);
    }

    public JsonValue execImageRecogQuery(List<String> scenes, String rootElement, String sex) {

	String queryString = SparqlQueryHelper.buildImageRecogQuery(scenes, rootElement, sex);
	return queryOntology(scenes, queryString);
    }

    protected JsonValue queryOntology(List<String> scenes, String queryString) {
	System.out.println(queryString);
	Query query = QueryFactory.create(queryString);
	QueryExecution qexec = QueryExecutionFactory.create(query, model);
//	QueryExecution qexec = virtuosoCon.query(query);
	// Run Query against Server instead of Local File
//        QueryExecution qexec = QueryExecutionFactory.sparqlService("http://localhost:3030/fashion", query);
	ArrayList<String> links2FashionOntology = new ArrayList<String>();
	JsonBuilder jb = new JsonBuilder();
	jb.startArray();
	JsonObject initalJsonValue = new JsonObject();
	JsonObject jNow = null;
	try {
	    ResultSet results = qexec.execSelect();
	    String lastFashionItem = null; // this variable indicates whether the new line is also a new fashion item or
					   // just another linkage to the fashionOntology. In that case, they are
					   // accumulated in the "link2FashionOntology" list.
	    String lastScene = null;
	    Boolean firstItem = true;
	    while (results.hasNext()) {
		QuerySolution soln = results.nextSolution();
		RDFNode rdf;

		// filter out namespaces of URIs using Regex
		String[] values = new String[8];

		jNow = new JsonObject();
		// While it would be possible to add this in the else if above, the separating
		// of the same functionality ensures that the "fashionOntology" list is always
		// on top of the json respone.

		rdf = soln.get("gender");
		if (rdf != null)
		    values[1] = rdf.toString().replaceAll("((.*)#)", "");

		rdf = soln.get("scene");
		if (scenes != null)
		    values[2] = rdf.toString().replaceAll("((.*)#)", "");
		rdf = soln.get("fashionsubitems");
		values[0] = rdf.toString().replaceAll("((.*)#)", "");
		if (lastFashionItem == null) {
		    lastFashionItem = values[0];
		    lastScene = values[2];
		}

		rdf = soln.get("hasSubClass");
		values[3] = rdf.toString().replaceAll("\\^(.*)", "");
		rdf = soln.get("responseId");
		if (rdf != null)
		    values[5] = rdf.toString().replaceAll("\\^(.*)", "");
		if (firstItem) {

		    rdf = soln.get("requestClass");
		    initalJsonValue.put("requestClass", rdf.toString().replaceAll("((.*)#)", ""));

		    rdf = soln.get("requestId");
		    if (rdf != null)
			initalJsonValue.put("id", Integer.parseInt(rdf.toString().replaceAll("\\^(.*)", "")));
//		    jb.value(initalJsonValue);
		    firstItem = false;
		}

		rdf = soln.get("link");
		if (rdf != null && (values[0].contentEquals(lastFashionItem) && lastScene.contentEquals(values[2]))) {
		    values[4] = rdf.toString().replaceAll("((.*)#)", "");
		    links2FashionOntology.add(values[4]);
		}
		if (links2FashionOntology.size() > 0 && (!results.hasNext() || !lastFashionItem.contentEquals(values[0])
			|| !lastScene.contentEquals(values[2]))) {

		    jNow.put("fashionOntology", SparqlQueryHelper.arrayList2JsonArray(links2FashionOntology));
		    links2FashionOntology.clear();
		    values[4] = rdf.toString().replaceAll("((.*)#)", "");
		    links2FashionOntology.add(values[4]);
		    if (values[5] != null)
			jNow.put("id", Integer.parseInt(values[5]));
		    lastFashionItem = values[0];
		    lastScene = values[2];
		    jNow.put("fashionElement", values[0].toLowerCase());
		    jNow.put("gender", values[1]);
		    // If the scene is left empty in the request, it is also not available in the
		    // response due to computational limitations
		    if (values[2] != null)
			jNow.put("scene", values[2]);
		    jNow.put("hasSubClass", SparqlQueryHelper.convertStringIntToBool(values[3]));

		    jb.value(jNow);
		}
		if (rdf == null) {
		    if (values[5] != null)
			jNow.put("id", Integer.parseInt(values[5]));
		    jNow.put("fashionElement", values[0].toLowerCase());
		    jNow.put("genders", values[1]);
		    if (values[2] != null)
			jNow.put("scenes", values[2]);
		    jNow.put("hasSubClass", SparqlQueryHelper.convertStringIntToBool(values[3]));
		    jb.value(jNow);

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    qexec.close();
	}
	jb.finishArray();
	initalJsonValue.put("results", jb.build());
	return initalJsonValue;
    }

}