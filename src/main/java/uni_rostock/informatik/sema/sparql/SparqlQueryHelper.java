package uni_rostock.informatik.sema.sparql;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.atlas.json.JsonArray;

/***
 * Provides some Helper-functions for the building and query of the ontology
 * 
 * @author achim.reiz@uni-rostock.de
 *
 */
public class SparqlQueryHelper {

    public static String buildImageRecogQuery(List<String> scenes, String rootElement, String sex) {
	StringBuilder queryBuilder = new StringBuilder();
	queryBuilder.append(SparqlQueryHelper.getOntologyHeader());
	if (!rootElement.matches("\\d{1,2}"))
	    queryBuilder.append("    BIND (sema:"
		    + rootElement
		    + " AS ?requestClass)\r\n");
	queryBuilder.append("    {\r\n"
		+ "      SELECT DISTINCT ?scene ?allfashion ?gender ?type \r\n"
		+ "      WHERE {\r\n");

	// Adding a List of corresponding scenes to all possible items is to
	// timeconsuming for the algorithm.

	if (scenes.size() != 0)
	    queryBuilder.append("        ?scene rdfs:subClassOf+ sema:scenes ;\r\n"
		    + "              #z & x are anonymous class consisting of object properties.\r\n"
		    + "        rdfs:subClassOf [ owl:onProperty sema:hasOccasion ; owl:someValuesFrom ?Occasion ] .\r\n");
	queryBuilder.append("      # Replace Fashion with filter item    \r\n"
		+ "      ?allfashion rdfs:subClassOf+ sema:IMAGERECOGNITION;\r\n");
	queryBuilder.append(SparqlQueryHelper.getSparqlMiddle());

	queryBuilder.append(scenes2Filter(scenes));

	if (!(sex == null || sex == ""))
	    queryBuilder.append("    . FILTER (?gender = sema:"
		    + sex
		    + ")\r\n");

	queryBuilder.append(SparqlQueryHelper.getOntologySubPart(rootElement));
	queryBuilder.append(getOntologyFooter());
	return queryBuilder.toString();
    }

    public static String buildFashionQuery(List<String> scenes, String rootElement, String sex, String type) {
	StringBuilder queryBuilder = new StringBuilder();
	queryBuilder.append(SparqlQueryHelper.getOntologyHeader());
	queryBuilder.append("    BIND (fashion:"
		    + rootElement
		    + " AS ?requestClass)\r\n");
	queryBuilder.append("    {\r\n"
		+ "      SELECT DISTINCT ?scene ?allfashion ?gender ?type \r\n"
		+ "      WHERE {\r\n");

	// Adding a List of corresponding scenes to all possible items is to
	// timeconsuming for the algorithm.

	if (scenes.size() != 0)
	    queryBuilder.append("        ?scene rdfs:subClassOf+ sema:scenes ;\r\n"
		    + "              #z & x are anonymous class consisting of object properties.\r\n"
		    + "        rdfs:subClassOf [ owl:onProperty sema:hasOccasion ; owl:someValuesFrom ?Occasion ] .\r\n");
	queryBuilder.append("      # Replace Fashion with filter item    \r\n"
		+ "      ?allfashion rdfs:subClassOf+ fashion:fashion;\r\n");
	queryBuilder.append(SparqlQueryHelper.getSparqlMiddle());

	queryBuilder.append(scenes2Filter(scenes));

	if (!(sex == null || sex == ""))
	    queryBuilder.append("    . FILTER (?gender = sema:"
		    + sex
		    + ")\r\n");

	queryBuilder.append(SparqlQueryHelper.getOntologySubPart(rootElement));
	queryBuilder.append(getOntologyFooter());
	return queryBuilder.toString();
    }

    /**
     * Prints the necessary Prefixes for accessing the items of the fashion and
     * image recognition ontology
     * 
     * @return PREFIX for Ontology
     */
    private static String getOntologyHeader() {
	return "PREFIX fashion: <http://sema.informatik.uni-rostock.de/fashion#>\r\n"
		+ "PREFIX rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\r\n"
		+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n"
		+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\r\n"
		+ "PREFIX sema: <http://sema.informatik.uni-rostock.de/imagerecog#>\r\n"
		+ "\r\n"
		+ "SELECT  DISTINCT ?scene ?fashionsubitems ?gender ?type ?link ?hasSubClass ?requestId ?requestClass ?responseId\r\n"
		+ "  WHERE\r\n"
		+ "  {\r\n"
		+ "\r\n";
    }

    private static String getSparqlMiddle() {
	return "        rdfs:subClassOf+ ?OccasionLink ; \r\n"
		+ "        rdfs:subClassOf+ ?typeLink ;\r\n"
		+ "        rdfs:subClassOf+ ?genderLink .\r\n"
		+ "      ?OccasionLink owl:onProperty sema:hasOccasion ;\r\n"
		+ "      owl:someValuesFrom ?Occasion . \r\n"
		+ "      ?typeLink owl:onProperty sema:hasType ;\r\n"
		+ "      owl:allValuesFrom ?type .\r\n"
		+ "      ?genderLink owl:onProperty sema:hasGender ; owl:allValuesFrom ?gender\r\n"
		+ "        \r\n"
		+ "        \r\n";
    }

    /***
     * Closes the Sparql request
     * 
     * @return String
     */
    private static String getOntologySubPart(String rootElement) {
	StringBuilder sb = new StringBuilder();
	sb.append("      }\r\n"
		+ "    } .\r\n"
		+ "    \r\n"
		+ "    ?fashionsubitems rdfs:subClassOf ?requestClass .\r\n"
		+ "    OPTIONAL {?fashionsubitems rdfs:subClassOf [owl:onProperty sema:isLinkableWith ; owl:someValuesFrom ?link] } .\r\n"
		+ "    OPTIONAL {?requestClass sema:imageRecogId ?requestId } .\r\n"
		+ "    OPTIONAL {?fashionsubitems sema:imageRecogId ?responseId } .\r\n");
	if (rootElement.matches("\\d{1,2}"))
	    sb.append("    FILTER(?requestId = "
		    + rootElement
		    + ") . \r\n");
	return sb.toString();
    }

    private static String getOntologyFooter() {
	return "    ?allfashion rdfs:subClassOf* ?fashionsubitems\r\n"
		+ "    BIND(EXISTS{?subClass rdfs:subClassOf ?fashionsubitems}\r\n"
		+ "      AS ?hasSubClass )\r\n"
		+ "\r\n"
		+ "    }\r\n"
		+ "ORDER BY ?scene ?fashionsubitems\r\n"
		+ "Limit 500";
    }

    /***
     * translates the legacy-type names to the one used in the main fashion
     * Ontology.
     * 
     * @param type can be "whole", "top" or "bottom"
     * @return "whole_type", "top_type", "bottom_type" or "" if not matched with a
     *         valid input
     */
    public static String cleanType(String type) {
	switch (type) {
	case "top":
	case "top_type":
	    type = "top_type";
	    break;
	case "bottom":
	case "bottom_type":
	    type = "bottom_type";
	    break;
	case "whole":
	case "whole_type":
	    type = "whole_type";
	default:
	    type = "";
	    break;
	}
	return type;
    }

    /***
     * Translates an arraylist to a JSON-Array
     * 
     * @param arrayList an ArrayList
     * @return Object of type JsonArray
     */
    public static JsonArray arrayList2JsonArray(ArrayList<String> arrayList) {
	JsonArray jsonArray = new JsonArray();
	for (String string : arrayList) {
	    jsonArray.add(string);
	}
	return jsonArray;
    }
    protected static String scenes2Filter(List<String> scenes) {
	StringBuilder queryBuilder = new StringBuilder();
	for (int i = 0; i < scenes.size(); i++) {
	    if (i == 0)
		queryBuilder.append("    . FILTER (?scene = sema:"
			+ scenes.get(i));
	    else
		queryBuilder.append("|| ?scene = sema:"
			+ scenes.get(i));
	    if (i == scenes.size() - 1)
		queryBuilder.append(")");
	}
	return queryBuilder.toString();
    }

    /***
     * Converts String "1" or "0" to bool value
     * 
     * @param value String containing "1" or "0"
     * @return Boolean value
     */
    static Boolean convertStringIntToBool(String value) {
        if (value.contentEquals("1") || value.contentEquals("true"))
            return true;
        else
            return false;
    }

}
