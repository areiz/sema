# Changelog and Instructions

## Accessing the Help

The help is available at `version/HowTo.jsp` on the server.

## Changelog

|Version|Changes|
|---|---|
|*v0.1*|Initial Version of Fashion Ontology. No Image-Recognition Service included. |
|*v0.2*|Image-Recognition Services added. |
|*v0.3*|Scenes can be left out. `sex` is changed to `gender` in the Request. In the response, `fashion` is altered to `fashionElement` to align the elements of request and response. <br />The fashionOntology-list is now at the top of the JSON file|
|*v0.4*|``scene`` can now be empty. Preliminary Error-Handling is added. inputs are now caps-insensitive. All image-recognition values are returned low-caps, the rest is returned small-caps |
|**v0.5**| Improved Performance by switching from native Jena to [virtuoso](http://vos.openlinksw.com/owiki/wiki/VOS) <br /> Changed naming of scene -> scenes and gender -> genders. Gender now also is bound to accept an array instead of a single value (see below for detailed query instructions)


***Encoding***: **Current Version** , *Legacy*

# Overview of Image Recognition Service

The three members of this project, [Rostock University, Chair of Business Informatics](wirtschaftsinformatik.uni-rostock.de), [Fraunhofer IGD](https://www.igd.fraunhofer.de/) and [FutureTV Group](https://www.futuretv-group.com/) all have different responsibilities regarding their work packages. Fraunhofer provides the image recognition technology, Rostock University allows the semantic reasoning and FutureTV Group orchestrates these services. More information on the general process and the motivation behind this project can be found on the following [link](https://www.wirtschaftsinformatik.uni-rostock.de/forschung/projekte/land-m-v/semantische-assistenz-bei-der-objekterkennung-in-bewegtbildern/).

![](Services.png)

The goal of the section is to show the technical process behind the newly developed technology and the dependencies and relations between the developed services

## Process of Image Recognition
This image recognition service is a tightly coupled system of the three project partners. The following section gives an overview of the orchestration of the services.
![](Orchestration1.png)
In the first step, the FutureTv sends the video to the Fraunhofer image recognition service. Here, the initial detection of the scenes and basic content data is carried out. This contains the scene of the video *(a scene is the setting of the main object e.g. an airfield, a bar or gym etc.)*
and basic information like the gender of the person that is shown in the video and the relevant body areas *(upper or lower body area or the whole body)*.
In the next step, this data gets returned to the FutureTV server and forwarded to the semantic reasoning service of Rostock University using the formalization shown below. This service utilizes the programmed Fashion-Knowledgebase to infer fashion items that can be worn in a given situation.
![](Orchestration3.png)
As an example, the combination of a business scene and sport trousers are highly unlikely and therefore excluded.
The results are returned to the FutureTV server, which triggers a new iteration to the Fraunhofer image recognition server. The information by the semantic service now enables a more efficient analysis as certain items can be excluded based on the given context. The process starts again, providing a refined result set with every iteration.
![](Orchestration5.png)
The refinement is taken place on a shared taxonomy. This enables the same wording for the same concepts along with all project partners. It is not necessary to always use the full depth of the tree - FutureTv can always finish the process as soon as the quality of the results is sufficient for their application case. Further, Rostock University provides not only semantic knowledge to the shared concepts, but also has connected this rather small ontology to a large conceptualization with more than 650 fashion items.
![The shared Taxonomy between Fraunhofer, Rostock University and FutureTv](imageRecog.png)

## Used Technology Stack for semantic reasoning
The semantic service runs on a virtual machine locally in the data center of the faculty for informatics and electrical enginering. The application itself is based on Java EE and runs on a Apache Tomcat server. Apache Jena is the Java library used for handling the ontology files in the web ontology language format (owl).

![](TechnologyStack.png)


Accessing Data on the SEMA-API
========
The following section describes the semantic interface of Rostock University. 

## The two API-Endpoints 
The API offers two endpoints for accessing the ontological knowledge - one for the large conceptual knowledge base, here called **fashion**-ontology, and one that is aligned in its terminology with the image recognition services offered by [Fraunhofer IGD](https://www.igd.fraunhofer.de), here called **image-recognition** ontology. Both are very similar regarding their request and response structure.

The current URL for the web service is <HTTP://sema.informatik.uni-rostock.de>, following the version-tag, the keyword `/webapi` and the endpoint of choosing. The image-recognition endpoint is available at `/imagerecog`. The large fashion ontology is available at `/fashion`.
This accumulates to e.g. `http://sema.informatik.uni-rostock.de/v0.3/webapi/fashion` for the fashion ontology. Further, the web-service allows the usage of ***POST*** in combination with the serialization ***JSON***.

## Accessing the Image Recognition Ontology
As stated above, the knowledge of the image recognition ontology is available at `version/webapi/imagerecog`. The HTTP-method ***POST*** and the following query structure is required:

### REQUEST


    {
        "genders": ["female"],
        "fashionElement": "",
        "scenes": [
            {
                "name": "aquarium",
                "possibility": 0.8
            }
        ]
    }

`fashionElement` is the element that is responsible for traversing the ontological hierarchy. It allows an iterative way of finding categories and sub-categories of the required items with the necessary depth. If it is not used or left empty e.g. `"fashionElement": ""`, the query starts at the root element of the ontology. `genders` can either be `male` or `female`. Starting with version number *0.4*, the gender has to be encoded in an Json, array, resulting in `"genders": ["female"]`. It is now also possible to provide two genders at once: `"genders": ["female", "male"]`. The same behavior can be achived if the whole `genders` attribute is omitted.

`scenes` contains a list of detected scenes by the Fraunhofer API. These scenes are also harmonized regarding their terminology. The detected `possibility` is currently not used for calculations but included for future research purposes.

The whole `scenes` section can also be omitted or left the array can be left empty `"scenes": []`. In that case, just the filter gender is applied In this case, due to limitations in computational power, the scenes are also not shown in the response.

### RESPONSE
The request stated above triggers the following response:

    [
    {
        "fashionElement": "Accessory",
        "genders": "female",
        "scenes": "aquarium",
        "hasSubClass": "true"
    },
    {
        "fashionElement": "Dress",
        "genders": "female",
        "scenes": "aquarium",
        "hasSubClass": "true"
    }
    ]
This JSON-Array contains all elements that were identified using the the filter *female*, within the scene *aquarium* beginning from the top category. To step in the next hierarchy-level, one can just take the idenfied `"fashionElement"`, e.g. *Dress* and perform the next request with `"fashion": "Dress"`. For doing so, `hasSubClass` gives additional information whether the element has a further Sub-Class or not. If this value changes to `false`, performing another query is dispensable.

### Connecting Image-Recognition and Fashion Ontology

The Image-Recognition maps all available detectors. Summed up, this accumulates to a little more than 60 elements, while the large fashion-ontology contains knowledge on over 650 items. When the traversal of the image-recognition ontology reaches the last element, this is shown with the `hasSubClass`-Element. Additionally, the JSON contains a `fashionOntology`-Item contains JSON-Array of items in the **fashion** ontology that matches to the Fashion-Elements in the **Image-Recognition** Ontology.

    [
    {
        "fashionOntology": [
            "baby-doll",
            "jersey-dresses",
            "summer-dresses",
            "sundresses"
        ],
        "fashionElement": "Short",
        "genders": "female",
        "scenes": "aquarium",
        "hasSubClass": "false"
    }
    ]

## Accessing the fashion Ontology

The API is accessible on sema.informatik.uni-rostock.de/VERSION/webapi/fashion.
The general request structure is similar to accessing the [Image-Recognition Ontology](#accessing-the-image-recognition-ontology):


    {
        "gender": "male",
        "type": "top",
        "fashionElement": "fashion",
        "scenes": [
            {
                "name": "airfield",
                "possibility": 0.8
            },
            {
                "name": "bar",
                "possibility": 0.4
            }
        ]
    }

Additionally, the element `type` can be provided. This can either be `top`, `bottom` or `whole` and links to the respective elements in the image recognition ontology

|Image Recognition|Fashion|
|---|---|
|top|top|
|bottom|bottom|
|dress|whole|

Like above, `fashion` is the element that is responsible for traversing the ontology. It allows an iterative way of finding categories and sub-categories of the required items with the necessary depth. To access it from the root element, one has to type ``"fashionElement": "fashion"`` for checking whether the items of interests are within any of the categories `clothes`, `accessories` or `shoes`. The next request could then include `"fashionElement": "shoes"` to find the next sub-category of shoes that are of interest.   