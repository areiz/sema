FROM tomcat:8.5

MAINTAINER Achim Reiz <achim.reiz@uni-rostock.de>

COPY ontometrics.war /usr/local/tomcat/webapps/ROOT.war